# Getting started

## Start minikube
minikube start

## Ckeck Cluster
kubectl cluster-info

## Get all pods in the cluster
kubectl get pods -A

## Init Dashboard
minikube dashboard